import cv2
from mtcnn.mtcnn import MTCNN
import gender_prediction
import predict_age

image = cv2.imread("/home/shushant/Desktop/anis.png")
face_detector = MTCNN()
faces = face_detector.detect_faces(image)
if faces:
    for idx, face in enumerate(faces):
        if faces[idx]['confidence'] >= 0.95:
            b = face.get('box')
            crop_face = image[b[1]:b[1]+b[3], b[0]:b[0]+b[2]]
            gender = gender_prediction.genderClassify(crop_face)
            age = predict_age.ageClasify(crop_face)
            cv2.imshow(str(gender), crop_face)
            print(gender, age)
            cv2.waitKey()
        else:
            print("Low Face Accuracy")
else:
    print("no face detected")