import os
import cv2
import numpy as np

from flask import Flask, request,jsonify
from flask_cors import CORS

from mtcnn.mtcnn import MTCNN
import gender_prediction
import predict_age


app = Flask(__name__)
CORS(app, headers="X-CSRFToken, Content-Type")

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

face_detector = MTCNN()

@app.route("/gender", methods=["POST", "GET"])
def getAge():
	for upload in request.files.getlist("file"):
		img_array = np.array(bytearray(upload.read()), dtype=np.uint8)
		import pdb;pdb.set_trace()	
		frame = cv2.imdecode(img_array, -1)
		faces = face_detector.detect_faces(frame)
		if faces:
		    for idx, face in enumerate(faces):
		        if faces[idx]['confidence'] >= 0.95:
		            b = face.get('box')
		            crop_face = frame[b[1]:b[1]+b[3], b[0]:b[0]+b[2]]
		            gender = gender_prediction.genderClassify(crop_face)
		            age = predict_age.ageClasify(crop_face)
		            # cv2.imshow(str(gender), crop_face)
		            # print(gender, age)
		            # cv2.waitKey()
		        else:
		            print("Low Face Accuracy")
		else:
		    print("no face detected")
	# response = {
	#             'gender':gender,
	#             'age':age,
	#             'recommended_items':filtered_items
	#             }
	return jsonify("Helo")




if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8001, debug=True)
