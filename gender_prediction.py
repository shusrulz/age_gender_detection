import cv2
import numpy as np
from keras.models import load_model

# load gender classificiation model
gender_model = load_model('models/gender_detection.model')
genderList = ['Male','Female']

def genderClassify(face_crop): # gender classification function
    face_crop = cv2.resize(face_crop, (96,96))
    face_crop = face_crop.astype("float") / 255.0
    face_crop = np.expand_dims(face_crop, axis=0)

    conf = gender_model.predict(face_crop)[0]

    # get label with max accuracy
    idx = np.argmax(conf)
    gender = genderList[idx]

    confidence = conf[idx] * 100
    if confidence >= 80:
        return (gender, confidence)
    else:
        return None
